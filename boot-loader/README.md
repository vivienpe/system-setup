Boot loader : GRUB2
===================

Requirements
------------
- pacman
- grub
- grub-customizer
- os-prober (for dual-boot)

Files description
-----------------
- `install.sh` : install script
- `background.jpeg` : background image for boot menu

Installation process
--------------------
1. Install required packages
1. launch install.sh : `./install.sh`

