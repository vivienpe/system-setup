#!/bin/sh
required_packages="grub grub-customizer"

alias echoi='echo -ne "\033[1m[INFO]\033[0m " && echo'
alias echow='echo -ne "\033[1m[\033[1;33mWARN\033[0m\033[1m]\033[0m " && echo'
alias echoe='echo -ne "\033[1m[\033[1;31mWARN\033[0m\033[1m]\033[0m " && echo'

set -e

echoi "Install boot loader config (GRUB2)"
echoi "Check if required packages are installed"
if ! pacman -Qi $required_packages > /dev/null; then
	echoe "Required packages is missing : $required_packages"
	exit 1
fi
if ! pacman -Qi "os-prober" > /dev/null; then
	echow "os-prober is needed for dual boot and missing"
fi
grub-customizer > /dev/null 2> /dev/null
