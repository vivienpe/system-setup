Editor : vim
============

Requirements
------------
- pacman
- gvim
- curl
- for YouCompleteMe pluggin :
    - cmake,
    - python3, gcc (base-devel on archlinux)

Files description
-----------------
- `main-user.vim` : configuration for users
- `vimrc-user` : source `main-user.vim`. To be placed to `~/.vimrc`
- `shortcuts.vim` : custom shortcuts
- `install.sh` : install script

Installation process
--------------------
1. Install required packages
1. launch install.sh : `./install.sh`

