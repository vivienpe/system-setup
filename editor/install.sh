#!/bin/sh
required_packages="gvim curl cmake python3 gcc"

alias echoi='echo -ne "\033[1m[INFO]\033[0m " && echo'
alias echow='echo -ne "\033[1m[\033[1;33mWARN\033[0m\033[1m]\033[0m " && echo'
alias echoe='echo -ne "\033[1m[\033[1;31mWARN\033[0m\033[1m]\033[0m " && echo'

set -e

echoi "Install editor config (vim)"
echoi "Check if required packages are installed"
if ! pacman -Qi "$required_packages" > /dev/null; then
	echoe "Required packages is missing : $required_packages"
	exit 1
fi
echoi "Install vim-plug"
curl -sfLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim 
echoi "Copy user config file"
cp vimrc-user ~/.vimrc
echoi "Set configuration directory"
sed -i "s+^let configpath = .*$+let configpath = '$PWD/'+" ~/.vimrc
echoi "Install vim pluggins"
# TODO update plugins ?
vim +PlugInstall +qall
echoi "Install pluggin YCM. Please wait"
cd ~/.vim/plugged/youcompleteme/
python3 install.py > /dev/null
