" Set azerty leader
let mapleader = ";"
let maplocalleader = ";"

" Global
inoremap jk <Esc>
noremap H ^
noremap L $

" OCaML documentation
augroup OCaML_ShortCuts
	autocmd!
	autocmd Filetype ocaml nnoremap <buffer> <LocalLeader>h :MerlinDocument<CR>
augroup end

" Syntastic next and previous Error/Warning and close
nnoremap <Leader>e :lnext<CR>
nnoremap <Leader>E :lprevious<CR>
nnoremap <Leader>ee :lfirst<CR>

" Force training
inoremap <Esc> <Nop>
noremap <Up> <Nop>
noremap <DOWN> <Nop>
noremap <Left> <Nop>
noremap <Right> <Nop>
