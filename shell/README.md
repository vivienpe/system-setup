Shell : zsh
===========

Requirements
------------
- pacman
- zsh
- zsh-completions

Files description
-----------------
- `global-zshrc` : default path /etc/zsh/zshrc
- `user-zshrc` : default path ~/.zshrc
- `install.sh` : install script

Installation process
--------------------
1. Install required packages
1. launch install.sh : `./install.sh`

