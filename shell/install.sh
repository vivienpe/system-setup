#!/bin/sh
required_packages="pacman zsh zsh-completions"

alias echoi='echo -ne "\033[1m[INFO]\033[0m " && echo'
alias echow='echo -ne "\033[1m[\033[1;33mWARN\033[0m\033[1m]\033[0m " && echo'
alias echoe='echo -ne "\033[1m[\033[1;31mWARN\033[0m\033[1m]\033[0m " && echo'

set -e

echoi "Install shell config (zsh)"
echoi "Check if required packages are installed"
if ! pacman -Qi $required_packages > /dev/null; then
	echoe "Required packages is missing : $required_packages"
	exit 1
fi
echoi "Copy global config file"
if [[ $EUID -ne 0 ]]; then
	echow " Skipped : you need to be root to change global config"
else
	cp ./global-zshrc /etc/zsh/zshrc
fi
echoi "Copy user config file"
cp ./user-zshrc ~/.zshrc
